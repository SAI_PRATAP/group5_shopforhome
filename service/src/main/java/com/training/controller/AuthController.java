package com.training.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.training.dto.AuthDTO;
import com.training.dto.UserDTO;
import com.training.model.UserEntity;
import com.training.service.AuthService;
import com.training.service.UserService;
import com.training.util.JwtUtil;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RestController
@CrossOrigin(origins = "http://localhost:4200/")
public class AuthController {
	@Autowired
	AuthService authService;
	@Autowired
	UserService userService;
	@Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private AuthenticationManager authenticationManager;
	
	
	
	@PostMapping("/login")
	public AuthDTO generateToken(@RequestBody UserDTO authRequest) throws Exception {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword())
            );
        } catch (Exception ex) {
            throw new Exception("invalid username/password");
        }
        AuthDTO authResp=new AuthDTO();
        UserEntity user=userService.getUserByUsername(authRequest.getUsername());
        authResp.setRole(user.getRole());
        authResp.setAuthToken( jwtUtil.generateToken(authRequest.getUsername()));
        return authResp;
       
    }
	@PostMapping("register")
	public ResponseEntity<String> registerUser(@RequestBody UserEntity user)  {
String message="";
		try {
			Integer userId=authService.registerUser(user);
			message="User registered successfully";
			return ResponseEntity.status(HttpStatus.OK).body(message);
		}catch(Exception e) {
			message=e.getMessage();
			  return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
		}
		
	}
}
