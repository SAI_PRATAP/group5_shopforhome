package com.training.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.training.model.UserEntity;


@Repository
public interface UserRepo extends JpaRepository<UserEntity, Integer> {
	
	 List<UserEntity> findByRole(String role);
	List<UserEntity> findByUserId(Integer userId);
//	List<UserEntity> findByUsername(String username);
	UserEntity findByUsername(String username);
}