import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestService } from '../service/rest.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  public products: any;
  public users: any;
  retData: any;
  constructor(private http: HttpClient,
    private router: Router,
    private rest:RestService) { }

  ngOnInit(): void {
    this.getUsers()
  }

  getUsers(){
    this.rest.getUsers().subscribe(res=>{
      this.users=res;
      this.retData=res;
    });
  }

  editUser(event: any){
    this.router.navigate(['/manage/users', event.target.id as number]);
  }

  async deleteUser(event: any){
    if(confirm('Are you sure you want to delete this user?')){
      const headers = { 'Accept': 'application/json', 'Content-Type': 'application/json' };
        this.http.delete<any>('http://localhost:8080/delete/?userId='.concat(event.target.id), {headers:this.rest.authHeaders()}).subscribe(data => {
          this.retData = this.getUsers();
      })
    }
  }

}
